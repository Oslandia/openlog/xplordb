## Validation

Beyond the normal database rules such as a numeric column must have only numbers in it etc., tables have constraints placed on them to help ensure the integrity of data, these include. 

  * Various foreign key constraints to maintain data consistency. Example - lithology logging codes can only be codes contained in the ref_lith table. Example - there must be a matching hole_id in collar table (dh_collar) and the drilling tables (e.g. dh_alteration). 
  * Primary key and unique key constraints to ensure data isn't duplicated. 
  * Check constraints to validate data. Example - percentages must be a positive number 100 or less. Example - Down hole depths must be less than or equal to the maximum depth of the hole. Example from_m must be less than to_m. 
  * Overlapping check constraint. Example - there must not be overlapping intervals in the core recovery table (dh.core_recovery). 

Also there are some SQL queries written as views to validate data, these are in \"v\" schema. 

Keep in mind that constrains and validation as suggested above won't guarantee that your data is 100% perfect. Visual checking of spatial data in QGIS 2D and or 3D may also identify errors in the data. 

