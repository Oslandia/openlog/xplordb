## Summary

[xplordb](https://sourceforge.net/projects/xplordb) an [open source](http://en.wikipedia.org/wiki/Open_source_software) mineral exploration database template for [PostgreSQL](http://www.postgresql.org)/ [PostGIS](http://postgis.refractions.net). xplordb is designed to store drilling, drill traces, surface sampling, assay, geology, and QAQC details and more. Customisation is possible. 

Geographic information systems (GIS) data/ layers can be directly imported to the main database. This integrated storage allows ease of access and sophisticated spatial SQL interrogation for better decision making. 

xplordb is self-documented, i.e. the documentation of the template is stored in the database. An outline of how the recommended software can be installed and an overview is documented in this wiki. 

### **Software**

On Linux/GNU and BSD operating systems all software used is open source, on other systems the main components used to interface with the xplordb template are open source and are; 

  * [PostgreSQL](http://www.postgresql.org) is a powerful, open source object-relational database system. It has more than 15 years of active development and a proven architecture that has earned it a strong reputation for reliability, data integrity, and correctness. It runs on all major operating systems, including Linux, UNIX (AIX, BSD, HP-UX, SGI IRIX, Mac OS X, Solaris, Tru64), and Windows. (PostgreSQL website) 
  * [PostGIS](http://postgis.refractions.net) adds support for geographic objects to the PostgreSQL database. In effect, PostGIS "spatially enables" the PostgreSQL server, allowing it to be used as a backend spatial database for GIS. (PostGIS website)
  * [psql](https://www.postgresql.org/docs/current/app-psql.html) is the default and very useful PostgreSQL interactive terminal
  * [pgAdmin](http://www.pgadmin.org) is the suggested [GUI](http://en.wikipedia.org/wiki/Graphical_user_interface) front-end for xplordb
  *  [QGIS](http://www.qgis.org/) can be used to directly display or analyse the stored spatial layers in 2 and 3D and also loading spatial and non-spatial data
  
Various proprietary GIS and 3D mining packages can also interface with xplordb via the PostgreSQL odbc drivers, native support is increasing. 

### **Flow Chart**

![Flow Chart](http://sourceforge.net/p/xplordb/xdb/Main_Page/attachment/xdb_flowchart-0.87.png)

### **Supported Operating Systems**

All the software can be run on the major operating systems including GNU/ Linux, BSD, OS X, Windows 7, Windows 8 and WIndows 10 (Microsoft missed 9 not me). 

### **User Pre-requisites**

Target audience is database admins. Knowledge of SQL, GIS, exploration drilling/ sampling/ assaying techniques and geology. Also knowledge of the operating system of choice. 

### **License**

The xplordb project including all code, documentation and the information in this wiki is copyleft and released under the [GNU General Public License, version 3](http://www.opensource.org/licenses/gpl-3.0)

